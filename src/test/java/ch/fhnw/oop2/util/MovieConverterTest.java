package ch.fhnw.oop2.util;

import ch.fhnw.oop2.model.Country;
import ch.fhnw.oop2.model.Language;
import ch.fhnw.oop2.model.Movie;
import ch.fhnw.oop2.model.MovieDto;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class MovieConverterTest {

    @Test
    void dtoToMovie() {
        Movie movie = getMovie();
        MovieDto movieDto = movie.toDto();
        Assertions.assertEquals(movieDto.getTitle(), getDto().getTitle());
        Assertions.assertEquals(movieDto.getSeen(), getDto().getSeen());
        Assertions.assertEquals(movieDto.getGenre(), getDto().getGenre());
        Assertions.assertEquals(movieDto.getYear(), getDto().getYear());
        Assertions.assertEquals(movieDto.getRating(), getDto().getRating());
        Assertions.assertEquals(movieDto.getDuration(), getDto().getDuration());
        Assertions.assertEquals(movieDto.getBoxOffice(), getDto().getBoxOffice());
        Assertions.assertEquals(movieDto.getCost(), getDto().getCost());
        Assertions.assertEquals(movieDto.getCompany(), getDto().getCompany());
        Assertions.assertEquals(movieDto.getDirector(), getDto().getDirector());
        Assertions.assertEquals(movieDto.getCast(), getDto().getCast());
        Assertions.assertEquals(movieDto.getCountry().toLowerCase(), getDto().getCountry().toLowerCase());
        Assertions.assertEquals(movieDto.getLanguage(), getDto().getLanguage());
    }

    @Test
    void movieToDto() {
        MovieDto movieDto = getDto();
        Movie movie = movieDto.toMovie();
        Assertions.assertEquals(movie.getTitle(), getMovie().getTitle());
        Assertions.assertEquals(movie.isSeen(), getMovie().isSeen());
        Assertions.assertEquals(movie.getGenre(), getMovie().getGenre());
        Assertions.assertEquals(movie.getYear(), getMovie().getYear());
        Assertions.assertEquals(movie.getRating(), getMovie().getRating());
        Assertions.assertEquals(movie.getDuration(), getMovie().getDuration());
        Assertions.assertEquals(movie.getBoxOffice(), getMovie().getBoxOffice());
        Assertions.assertEquals(movie.getCost(), getMovie().getCost());
        Assertions.assertEquals(movie.getCompany(), getMovie().getCompany());
        Assertions.assertEquals(movie.getDirector(), getMovie().getDirector());
        Assertions.assertEquals(movie.getCast(), getMovie().getCast());
        Assertions.assertEquals(movie.getCountry(), getMovie().getCountry());
        Assertions.assertEquals(movie.getLanguage(), getMovie().getLanguage());
    }


    private Movie getMovie() {
        Movie movie = new Movie();
        movie.setTitle("Movie01");
        movie.setSeen(true);
        movie.setGenre("Horror");
        movie.setYear(2019);
        movie.setRating(4);
        movie.setDuration(260);
        movie.setBoxOffice(20000L);
        movie.setCost(90000L);
        movie.setCompany("Century Fox");
        movie.setDirector("Movie 1 Director");
        List<String> cast1 = new ArrayList<>();
        cast1.add("Brett Pitt");
        cast1.add("Jungle Depp");
        movie.setCast(FXCollections.observableList(cast1));
        movie.setCountry(new Country(Locale.US));
        movie.setLanguage(new Language(Locale.ENGLISH));
        return movie;
    }

    private MovieDto getDto() {
        MovieDto movie = new MovieDto();
        movie.setTitle("Movie01");
        movie.setSeen(true);
        movie.setGenre("Horror");
        movie.setYear(2019);
        movie.setRating(4);
        movie.setDuration(260);
        movie.setBoxOffice(20000L);
        movie.setCost(90000L);
        movie.setCompany("Century Fox");
        movie.setDirector("Movie 1 Director");
        List<String> cast1 = new ArrayList<>();
        cast1.add("Brett Pitt");
        cast1.add("Jungle Depp");
        movie.setCast(FXCollections.observableList(cast1));
        movie.setCountry("us");
        movie.setLanguage("en");
        return movie;
    }
}