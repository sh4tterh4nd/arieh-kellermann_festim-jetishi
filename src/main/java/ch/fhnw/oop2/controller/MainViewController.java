package ch.fhnw.oop2.controller;

import ch.fhnw.oop2.model.Country;
import ch.fhnw.oop2.model.Language;
import ch.fhnw.oop2.model.Movie;
import ch.fhnw.oop2.util.DataUtil;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class MainViewController {

    private static final int MAX_LENGTH_INTEGER = 9;
    private static final int MAX_LENGTH_LONG = 18;

    private ObservableList<Movie> allMovies;

    public TableView movieTable;
    public TableColumn<Movie, Boolean> seenColumn;
    public TableColumn<Movie, String> titleColumn;
    public TableColumn<Movie, Integer> yearColumn;
    public TableColumn<Movie, Integer> ratingColumn;
    public TableColumn<Movie, String> genreColumn;

    public ImageView movieImageView;
    public Label titleLabel;
    public Label summaryLabel;
    public CheckBox seenCheckbox;
    public Slider ratingSlider;

    public TextField titleTextField;
    public TextField yearTextField;
    public TextField genreTextField;
    public TextField companyTextField;
    public ComboBox<Language> languageCombobox;
    public TextField directorTextField;
    public TextField durationTextField;
    public ListView<String> castListView;
    public ComboBox<Country> countryCombobox;
    public TextField boxOfficeTextField;
    public TextField costTextField;
    public Label profitLabel;

    public Button deleteMovieButton;
    public Button moveCastUpButton;
    public Button moveCastDownButton;
    public Button deleteCastButton;
    public Button editCastButton;

    public MainViewController(List<Movie> movies) {
        allMovies = FXCollections.observableArrayList(movies);
    }

    @FXML
    public void initialize() {
        seenColumn.setCellValueFactory(new PropertyValueFactory<Movie, Boolean>("seen"));
        seenColumn.setCellFactory(tc -> new TableCell<Movie, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : item ? "\u2714" : "\u274C");
                setAlignment(Pos.CENTER);
            }
        });
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
        ratingColumn.setCellValueFactory(new PropertyValueFactory<>("rating"));
        ratingColumn.setCellFactory(tc -> new TableCell<>() {
            @Override
            protected void updateItem(Integer integer, boolean b) {
                super.updateItem(integer, b);
                StringBuilder text = new StringBuilder();
                if (integer != null) {

                    for (Integer i = 0; i < integer; i++) {
                        text.append("\uD83C\uDF1F");
                    }
                }
                setText(text.toString());
                setAlignment(Pos.CENTER);
            }
        });
        genreColumn.setCellValueFactory(new PropertyValueFactory<>("genre"));

        List<Language> languages = Arrays.stream(Locale.getISOLanguages())
                .map(Locale::new)
                .map(Language::new)
                .collect(Collectors.toList());
        languageCombobox.setItems(FXCollections.observableList(languages));

        List<Country> countries = Arrays.stream(Locale.getISOCountries())
                .map(c -> new Locale("", c))
                .map(Country::new)
                .collect(Collectors.toList());
        countryCombobox.setItems(FXCollections.observableList(countries));

        titleLabel.textProperty().bind(titleTextField.textProperty());
        summaryLabel.textProperty().bind(Bindings.createStringBinding(() -> {
            String durationString = "00 h 00 min";
            int timeInMinutes = Integer.parseInt(durationTextField.getText().isBlank() ? "0" : durationTextField.getText());
            durationString = String.format("%02d h %02d min", timeInMinutes / 60, timeInMinutes % 60);
            return String.format("%s \u2022 %s \u2022 %s", yearTextField.getText(), genreTextField.getText(), durationString);
        }, genreTextField.textProperty(), yearTextField.textProperty(),  durationTextField.textProperty()));

        profitLabel.textFillProperty().bind(Bindings.createObjectBinding(() -> {
            if (boxOfficeTextField.getText().isBlank() && costTextField.getText().isBlank()) {
                return Color.BLACK;
            }

            double expenses = Double.parseDouble(costTextField.getText());
            double profit = Double.parseDouble(boxOfficeTextField.getText()) - expenses;
            double winPercentage = (profit * 100) / expenses;
            if (winPercentage < 0) {
                return Color.RED;
            } else if (winPercentage > 0 && winPercentage < 10) {
                return Color.YELLOW;
            } else {
                return Color.GREEN;
            }
        }, boxOfficeTextField.textProperty(), costTextField.textProperty()));
        profitLabel.textProperty().bind(Bindings.createStringBinding(() -> {
            if (boxOfficeTextField.getText().isBlank() || costTextField.getText().isBlank()) {
                return "0 (0%)";
            }

            double expenses = Double.parseDouble(costTextField.getText());
            double profit = Double.parseDouble(boxOfficeTextField.getText()) - expenses;
            return String.format("%s (%.1f%%)", NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(profit), (profit * 100) / expenses);
        }, boxOfficeTextField.textProperty(), costTextField.textProperty()));

        deleteMovieButton.disableProperty().bind(Bindings.createBooleanBinding(() -> movieTable.getSelectionModel().getSelectedIndex() == -1, movieTable.getSelectionModel().selectedIndexProperty()));
        moveCastUpButton.disableProperty().bind(Bindings.createBooleanBinding(() -> castListView.getSelectionModel().getSelectedIndex() <= 0, castListView.getSelectionModel().selectedIndexProperty()));
        moveCastDownButton.disableProperty().bind(Bindings.createBooleanBinding(() -> castListView.getSelectionModel().getSelectedIndex() == castListView.getItems().size() - 1, castListView.getSelectionModel().selectedIndexProperty()));
        deleteCastButton.disableProperty().bind(Bindings.createBooleanBinding(() -> castListView.getSelectionModel().getSelectedIndex() == -1, castListView.getSelectionModel().selectedIndexProperty()));
        editCastButton.disableProperty().bind(Bindings.createBooleanBinding(() -> castListView.getSelectionModel().getSelectedIndex() == -1, castListView.getSelectionModel().selectedIndexProperty()));

        movieTable.setItems(allMovies);
        movieTable.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> onSelectMovie((Movie) oldValue, (Movie) newValue));

        if (allMovies.size() >= 1) {
            movieTable.getSelectionModel().selectFirst();
        }
    }

    public void saveModel(ActionEvent actionEvent) {
        Path data = Paths.get("data/films.json");
        try {
            DataUtil.saveMovies(allMovies, data);
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, String.format("%s %s", e.toString(), e.getMessage()), ButtonType.OK).showAndWait();
        }
    }

    public void addMovie(ActionEvent actionEvent) {
        movieTable.getItems().add(0, new Movie());
        movieTable.getSelectionModel().selectFirst();
    }

    public void deleteMovie(ActionEvent actionEvent) {
        Movie movieToDelete = (Movie) movieTable.getSelectionModel().getSelectedItem();

        if (movieToDelete != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete '" + movieToDelete.getTitle() + "' ?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
            if (alert.showAndWait().orElse(ButtonType.NO) == ButtonType.YES) {
                movieTable.getItems().remove(movieToDelete);
            }
        }
    }

    public void addCast(ActionEvent actionEvent) {
        TextInputDialog memberNameDialog = new TextInputDialog("member");
        memberNameDialog.setHeaderText("Enter the cast memeber name");
        memberNameDialog.setContentText("Name:");
        memberNameDialog.showAndWait().ifPresent(name -> castListView.getItems().add(0, name));
    }

    public void editCast(ActionEvent actionEvent) {
        TextInputDialog memberNameDialog = new TextInputDialog(castListView.getSelectionModel().getSelectedItem());
        memberNameDialog.setHeaderText("Enter the cast memeber name");
        memberNameDialog.setContentText("Name:");

        int selectedIndex = castListView.getSelectionModel().getSelectedIndex();
        memberNameDialog.showAndWait().ifPresent(name -> {
            castListView.getItems().remove(selectedIndex);
            castListView.getItems().add(selectedIndex, name);
            castListView.getSelectionModel().select(selectedIndex);
        });
    }

    public void deleteCast(ActionEvent actionEvent) {
        int indexToDelete = castListView.getSelectionModel().getSelectedIndex();
        castListView.getItems().remove(indexToDelete);
    }

    public void moveCastUp(ActionEvent actionEvent) {
        int selectedIndex = castListView.getSelectionModel().getSelectedIndex();
        String selectedValue = castListView.getSelectionModel().getSelectedItem();

        castListView.getItems().remove(selectedIndex);
        castListView.getItems().add(selectedIndex - 1, selectedValue);
        castListView.getSelectionModel().select(selectedIndex - 1);
        castListView.scrollTo(selectedIndex - 1);
    }

    public void moveCastDown(ActionEvent actionEvent) {
        int selectedIndex = castListView.getSelectionModel().getSelectedIndex();
        String selectedValue = castListView.getSelectionModel().getSelectedItem();

        castListView.getItems().remove(selectedIndex);
        castListView.getItems().add(selectedIndex + 1, selectedValue);
        castListView.getSelectionModel().select(selectedIndex + 1);
        castListView.scrollTo(selectedIndex + 1);
    }

    private void onSelectMovie(Movie previousMovie, Movie currentMovie) {
        if (previousMovie != null) {
            previousMovie.unbindAllProperties();
        }

        titleTextField.setText(currentMovie.getTitle());
        currentMovie.titleProperty().bind(titleTextField.textProperty());

        seenCheckbox.setSelected(currentMovie.isSeen());
        currentMovie.seenProperty().bind(seenCheckbox.selectedProperty());

        yearTextField.setText(String.valueOf(currentMovie.getYear()));
        yearTextField.setTextFormatter(getNumberTextFormatter(MAX_LENGTH_INTEGER));
        currentMovie.yearProperty().bind(Bindings.createIntegerBinding(() -> Integer.valueOf(yearTextField.getText()), yearTextField.textProperty()));

        genreTextField.setText(currentMovie.getGenre());
        currentMovie.genreProperty().bind(genreTextField.textProperty());

        durationTextField.setText(String.valueOf(currentMovie.getDuration()));
        durationTextField.setTextFormatter(getNumberTextFormatter(MAX_LENGTH_INTEGER));
        currentMovie.durationProperty().bind(Bindings.createObjectBinding(() -> Integer.valueOf(durationTextField.getText()), durationTextField.textProperty()));

        ratingSlider.setValue(currentMovie.getRating());
        currentMovie.ratingProperty().bind(Bindings.createIntegerBinding(() -> (int) ratingSlider.getValue(), ratingSlider.valueProperty()));

        boxOfficeTextField.setText(String.valueOf(currentMovie.getBoxOffice()));
        boxOfficeTextField.setTextFormatter(getNumberTextFormatter(MAX_LENGTH_LONG));
        currentMovie.boxOfficeProperty().bind(Bindings.createObjectBinding(() -> Long.valueOf(boxOfficeTextField.getText()), boxOfficeTextField.textProperty()));

        costTextField.setText(String.valueOf(currentMovie.getCost()));
        costTextField.setTextFormatter(getNumberTextFormatter(MAX_LENGTH_LONG));
        currentMovie.costProperty().bind(Bindings.createObjectBinding(() -> Long.valueOf(costTextField.getText()), costTextField.textProperty()));

        companyTextField.setText(currentMovie.getCompany());
        currentMovie.companyProperty().bind(companyTextField.textProperty());

        directorTextField.setText(currentMovie.getDirector());
        currentMovie.directorProperty().bind(directorTextField.textProperty());

        languageCombobox.setValue(currentMovie.getLanguage());
        currentMovie.languageProperty().bind(languageCombobox.getSelectionModel().selectedItemProperty());

        countryCombobox.setValue(currentMovie.getCountry());
        currentMovie.countryProperty().bind(countryCombobox.getSelectionModel().selectedItemProperty());

        castListView.setItems(currentMovie.getCast());
    }

    private TextFormatter<String> getNumberTextFormatter(int maxLen) {
        UnaryOperator<TextFormatter.Change> filter = change -> {
            String text = change.getControlNewText();

            if (text.matches("^\\s*$")) {
                change.setText("0");
                change.setCaretPosition(1);
                return change;
            }

            if (text.matches("^[0-9]{1," + maxLen + "}$") ) {
                int length = change.getControlNewText().length();
                if (length > maxLen) {
                    return null;
                }
                return change;
            }
            return null;
        };
        return new TextFormatter<String>(filter);
    }
}
