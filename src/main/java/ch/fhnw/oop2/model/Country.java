package ch.fhnw.oop2.model;

import java.util.Locale;
import java.util.Objects;

public class Country {

    private String countryCode;
    private String name;

    public Country(Locale locale) {
        this.countryCode = locale.getCountry();
        this.name = locale.getDisplayCountry(Locale.ENGLISH);
    }

    public Country(String countryCode, String name) {
        this.countryCode = countryCode;
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(countryCode, country.countryCode) &&
                name.equals(country.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryCode, name);
    }
}
