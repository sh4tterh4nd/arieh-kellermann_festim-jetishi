package ch.fhnw.oop2.model;

import java.util.Locale;
import java.util.Objects;

public class Language {
    private String langCode;
    private String name;

    public Language(Locale locale) {
        this.langCode = locale.getLanguage();
        this.name = locale.getDisplayLanguage(Locale.ENGLISH);
    }

    public Language(String langCode, String name) {
        this.langCode = langCode;
        this.name = name;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Language)) return false;
        Language language = (Language) o;
        return Objects.equals(langCode, language.langCode) &&
                name.equals(language.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(langCode, name);
    }
}
