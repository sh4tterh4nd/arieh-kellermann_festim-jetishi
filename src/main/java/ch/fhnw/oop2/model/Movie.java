package ch.fhnw.oop2.model;

import ch.fhnw.oop2.util.MovieConverter;
import javafx.beans.property.*;
import javafx.collections.ObservableList;

public class Movie {

    private ObjectProperty<Language> language = new SimpleObjectProperty<>();
    private ObjectProperty<Country> country = new SimpleObjectProperty<>();

    private StringProperty title = new SimpleStringProperty();
    private StringProperty genre = new SimpleStringProperty();
    private StringProperty director = new SimpleStringProperty();
    private StringProperty company = new SimpleStringProperty();
    private BooleanProperty seen = new SimpleBooleanProperty();
    private IntegerProperty rating = new SimpleIntegerProperty();
    private IntegerProperty year = new SimpleIntegerProperty();
    private ObjectProperty<Integer> duration = new SimpleObjectProperty<>(0);
    private ObjectProperty<Long> boxOffice = new SimpleObjectProperty<>(0L);
    private ObjectProperty<Long> cost = new SimpleObjectProperty<>(0L);
    private ObservableList<String> cast = new SimpleListProperty<>();

    public void unbindAllProperties() {
        title.unbind();
        genre.unbind();
        director.unbind();
        company.unbind();
        seen.unbind();
        rating.unbind();
        year.unbind();
        duration.unbind();
        boxOffice.unbind();
        cost.unbind();
        country.unbind();
        language.unbind();
    }

    public Language getLanguage() {
        return language.get();
    }

    public ObjectProperty<Language> languageProperty() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language.set(language);
    }

    public Country getCountry() {
        return country.get();
    }

    public ObjectProperty<Country> countryProperty() {
        return country;
    }

    public void setCountry(Country country) {
        this.country.set(country);
    }

    public boolean isSeen() {
        return seen.get();
    }

    public BooleanProperty seenProperty() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen.set(seen);
    }

    public int getRating() {
        return rating.get();
    }

    public IntegerProperty ratingProperty() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating.set(rating);
    }

    public int getYear() {
        return year.get();
    }

    public IntegerProperty yearProperty() {
        return year;
    }

    public void setYear(int year) {
        this.year.set(year);
    }

    public Integer getDuration() {
        return duration.get();
    }

    public ObjectProperty<Integer> durationProperty() {
        return duration;
    }

    public void setDuration(Integer duration) {
        if (duration == null) {
            duration = 0;
        }
        this.duration.set(duration);
    }

    public Long getBoxOffice() {
        return boxOffice.get();
    }

    public ObjectProperty<Long> boxOfficeProperty() {
        return boxOffice;
    }

    public void setBoxOffice(Long boxOffice) {
        if (boxOffice == null) {
            boxOffice = 0L;
        }
        this.boxOffice.set(boxOffice);
    }

    public ObservableList<String> getCast() {
        return cast;
    }

    public void setCast(ObservableList<String> cast) {
        this.cast = cast;
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getGenre() {
        return genre.get();
    }

    public StringProperty genreProperty() {
        return genre;
    }

    public Long getCost() {
        return cost.get();
    }

    public ObjectProperty<Long> costProperty() {
        return cost;
    }

    public void setCost(Long cost) {
        if (cost == null) {
            cost = 0L;
        }
        this.cost.set(cost);
    }

    public void setGenre(String genre) {
        this.genre.set(genre);
    }

    public String getDirector() {
        return director.get();
    }

    public StringProperty directorProperty() {
        return director;
    }

    public void setDirector(String director) {
        this.director.set(director);
    }

    public String getCompany() {
        return company.get();
    }

    public StringProperty companyProperty() {
        return company;
    }

    public void setCompany(String company) {
        this.company.set(company);
    }

    public MovieDto toDto() {
        return MovieConverter.movieToDto(this);
    }
}
