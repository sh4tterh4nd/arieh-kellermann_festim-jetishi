package ch.fhnw.oop2.util;

import ch.fhnw.oop2.model.Movie;
import ch.fhnw.oop2.model.MovieDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class DataUtil {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static List<Movie> loadMovies(String path) throws IOException {
        return loadMovies(Paths.get(path));
    }

    public static List<Movie> loadMovies(Path path) throws IOException {
        List<MovieDto> dtoList = objectMapper.readValue(path.toFile(), new TypeReference<List<MovieDto>>() {
        });
        return dtoList.stream().map(MovieDto::toMovie).collect(Collectors.toList());
    }

    public static void saveMovies(List<Movie> movies, Path path) throws IOException {
        List<MovieDto> moviesToSave = movies.stream().map(Movie::toDto).collect(Collectors.toList());
        objectMapper.writeValue(path.toFile(), moviesToSave);
    }
}
