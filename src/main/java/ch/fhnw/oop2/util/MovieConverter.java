package ch.fhnw.oop2.util;

import ch.fhnw.oop2.model.Country;
import ch.fhnw.oop2.model.Language;
import ch.fhnw.oop2.model.Movie;
import ch.fhnw.oop2.model.MovieDto;
import javafx.collections.FXCollections;

import java.util.Locale;

public class MovieConverter {

    public static Movie dtoToMovie(MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setBoxOffice(movieDto.getBoxOffice());
        movie.setCast(FXCollections.observableList(movieDto.getCast()));
        movie.setCompany(movieDto.getCompany());
        movie.setCost(movieDto.getCost());
        movie.setCountry(new Country(new Locale("", movieDto.getCountry())));
        movie.setDirector(movieDto.getDirector());
        movie.setDuration(movieDto.getDuration());
        movie.setGenre(movieDto.getGenre());
        movie.setLanguage(new Language(new Locale(movieDto.getLanguage())));
        movie.setRating(movieDto.getRating());
        movie.setSeen(movieDto.getSeen());
        movie.setTitle(movieDto.getTitle());
        movie.setYear(movieDto.getYear());
        return movie;
    }

    public static MovieDto movieToDto(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setBoxOffice(movie.getBoxOffice());
        movieDto.setCast(FXCollections.observableList(movie.getCast()));
        movieDto.setCompany(movie.getCompany());
        movieDto.setCost(movie.getCost());
        movieDto.setCountry(movie.getCountry().getCountryCode());
        movieDto.setDirector(movie.getDirector());
        movieDto.setDuration(movie.getDuration());
        movieDto.setGenre(movie.getGenre());
        movieDto.setLanguage(movie.getLanguage().getLangCode());
        movieDto.setRating(movie.getRating());
        movieDto.setSeen(movie.isSeen());
        movieDto.setTitle(movie.getTitle());
        movieDto.setYear(movie.getYear());
        return movieDto;
    }
}
