package ch.fhnw.oop2;

import ch.fhnw.oop2.controller.MainViewController;
import ch.fhnw.oop2.util.DataUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MovieRatingApplication {

    public static void main(String[] args) {
        Application.launch(App.class, args);
    }

    public static class App extends Application {
        public void start(Stage stage) throws IOException {
            Path data = Paths.get("data/films.json");
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainView.fxml"));
            MainViewController controller = new MainViewController(DataUtil.loadMovies(data));
            loader.setController(controller);

            Parent root = loader.load();
            Scene scene = new Scene(root, 1375, 900);

            stage.setTitle("Moviegated");
            stage.getIcons().add(new Image("/images/logo.png"));
            stage.setScene(scene);
            stage.show();
        }
    }
}
